# DevOps in Action

Esse é um projeto com alguns exemplos e exercícios para demonstrar a filosifa, cultura e práticas do movimento conhecido como DevOps.

A primeira recomendação para uso desse projeto é você realizar um fork do projeto antes de iniciar os estudos e exercícios, siga as instruções das etapas a seguir.

### Pré-requisitos

As ferramentas a seguir serão necessárias para a realização dos exercícios, então se certifique que todas estão instaladas e configuradas para uso:

- As atividades todas foram feitas e validadas usando Sistema operacional Linux ou OSX.

* Git
* Conta do Gitlab (configurar a chave SSH na sua conta)
* Docker
* Ansible

Para agilizar o dia do treinamento já baixe as imagens que serão utilizadas, execute os comandos abaixo para baixar:

- Download da imagem do Ubuntu com servidor de SSH ativado

> docker pull rastasheep/ubuntu-sshd:18.04

- Download da imagem do Jenkins, ferramenta bastante utilizada para suporte a integração contínua

> docker pull jenkins

- Download da imagem do PostgreSQL

> docker pull postgres

### Inicialização

Vamos preparar o ambiente de trabalho, siga as etapas a seguir:

1.  Faça o fork do projeto `devops-in-action` - (https://gitlab.com/pigor/devops-in-action);
2.  Faça o clone do repositório criado a partir do fork;
3.  Altere esse README no seu fork adicionando na sessão _Participantes_ o nome das pessoas que estão no seu grupo

### Exercícios

1.  Versionamento de Código usando Git
2.  Primeiro deploy usando apenas o controle de versão como apoio (deploy_1)
3.  Automatizar a configuração do servidor e deploy da aplicação com Ansible (deploy_2)
4.  Automatizar o build e a execução dos testes usando um servidor de integração contínua (Jenkins) (deploy_3)
5.  Adicionar o Monitoramento (monitor)
6.  Case de Gerenciamento de Base de Dados (migrar)
7.  Automatizar o pipeline para após o sucesso da build executar o deploy e a atualização do servidor de produção (entregar)

### Participantes
Lucas Mota
Sron Lemos
Efren 

### Exercício de Git

A segunda etapa desse exercício você vai ver como é realizada a criação de uma branch e como submeter as alterações da sua branch para revisão e integração em uma outra branch, então siga as etapas a seguir:

1 - Crie uma branch com o nome `checkpoints`, use o comando abaixo:

> git checkout -b checkpoints

2 - Na branch criada você vai criar um arquivo chamado `CHECKPOINTS.md` e nesse arquivo você vai adicionar os tópicos estudados e o que vocês aprenderam em cada tópico, como no exemplo abaixo:

```
Aprendizados:

- Contexto e história do movimento DevOps
- Alguns conceitos de Git
- ...
```

Esse arquivo será atualizado durante os próximo exercícios. Após criar o arquivo e adicionar os primeiros checkpoints vamos versioná-lo, siga os passos a seguir:

3 - Adicione o arquivo no versionamento:

> git add CHECKPOINTS.md

4 - E agora crie um commit dessa modificação:

> git commit CHECKPOINTS.md -m 'adicionado o arquivo com os checkpoints do aprendizado'

5 - Agora precisamos enviar essas modificações para o repositório remoto para compartilhar com os demais que tiverem acesso a esse projeto e para podermos solicitar a integração dessa modificação na branch principal, execute o comando a seguir:

> git push origin checkpoints

6 - Após a criação da branch remota `checkpoints` outras pessoas poderão acessar a sua branch e trabalhar nela também, mas agora vamos criar uma solicitação de integração do código dessa branch com a branch principal usando a interface da ferramenta que estamos usando para hospedar nossos repositórios remotos (gitlab, github, bitbucket, ou outro).

7 - Após a revisão (code review) e aprovação pela própria ferramenta é possível finalizar o merge e atualizar a branch principal (master), para validar vamos atualizar nossa branch master

> git checkout master

> git pull origin master

8 - As alterações aprovadas no merge/pull request irão aparecer na branch principal finalizando o fluxo.
